package de.schildbach.wallet.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import de.schildbach.wallet.Constants;
import de.schildbach.wallet.WalletApplication;
import de.schildbach.wallet_test.R;

import java.io.*;

public class ImportChainActivity extends Activity{
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;
    private Handler handler = new Handler();
    private File blackFile;
    private File blackChain;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume(){
       super.onResume();
       String blackFileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()
                +"/blackchain.dat";
       blackFile = new File(blackFileName);
       if(blackFile.exists())
            importBlackie();
       else{
           Log.d("ImportChainActivity","############### not found " + blackFileName);
           Intent intent = new Intent(ImportChainActivity.this, WalletActivity.class);
           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           getApplicationContext().startActivity(intent);
       }

    }

    private void importBlackie() {
        new ImportChain().execute();
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Detected chain, importing...");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }
    class ImportChain extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int len;
            try {
                long fileLength = blackFile.length();
                InputStream input =  new FileInputStream(blackFile);
                blackChain = new File(getDir("blockstore", Context.MODE_PRIVATE), Constants.Files.BLOCKCHAIN_FILENAME);
                OutputStream output = new FileOutputStream(blackChain);

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                long total = 0;
                while ((len = input.read(buf)) > 0) {
                    total += len;
                    publishProgress("" + (int)((total*100)/fileLength));
                    output.write(buf, 0, len);
                }
                output.flush();
                input.close();
                output.close();
                blackFile.delete();
            } catch (IOException e) {
                Log.d("problem reading assets, continuing without ", e.getMessage());
            }
            return null;

        }
        protected void onProgressUpdate(String... progress) {
            Log.d("Importing",progress[0]);
            mProgressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onCancelled(){
            if(blackChain!=null && blackChain.exists()){
                blackChain.delete();
            }
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
            super.onPostExecute(unused);
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    Intent intent = new Intent(ImportChainActivity.this, WalletActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            }, 1000);
        }
    }
}
